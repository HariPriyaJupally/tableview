//
//  SecondViewController.swift
//  TableViews! Rock
//
//  Created by Jupally,Hari Priya on 2/19/19.
//  Copyright © 2019 Jupally,Hari Priya. All rights reserved.
//

import UIKit

class TouristSitesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var touristSites = ["Denver", "Las Vegas", "Iowa", "Arizona", "Grand Canyon"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return touristSites.count
        }  else {
            return -1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "touristSites")!
        cell.textLabel?.text = touristSites[indexPath.row]
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

}

